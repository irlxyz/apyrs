from nose.tools import assert_equal

from apyrs.mice import MicEMessage
from apyrs.mice import encode_destination_address_field


def check_encode_destination_address_field(inputs, expected):
    assert_equal(encode_destination_address_field(*inputs), expected)


def test_encode_destination_address_field():
    tests = [
        (("332564", MicEMessage.M3, True, False, True, 0), "S32U6T"),
        (("332564", MicEMessage.M3, True, False, True, 3), "S32U6T-3"),
        (("332564", MicEMessage.M3, True, False, True, 11), "S32U6T-11"),
        (("332564", MicEMessage.C3, True, False, True, 11), "D32U6T-11"),
    ]
    for test in tests:
        yield check_encode_destination_address_field, test[0], test[1]
