from nose.tools import assert_almost_equal
from nose.tools import assert_equal

from apyrs.extension import CourseSpeed
from apyrs.extension import PowerHeightGain
from apyrs.extension import PreCalculatedRange


def test_rng_loop():
    for rng in range(0, 9999):
        assert_equal(str(PreCalculatedRange.from_content(f"RNG{rng:04.0f}")),
                     f"RNG{rng:04.0f}")


def test_phg_loop():
    for phgd in range(0, 9999):
        if phgd % 10 == 9:
            # Can't have 9 for directivity
            continue
        assert_equal(str(PowerHeightGain.from_content(f"PHG{phgd:04.0f}")),
                     f"PHG{phgd:04.0f}")


def check_csespd_str(given, expected):
    assert_equal(str(CourseSpeed(*given)), expected)


def check_csespd_parse(given, expected):
    cs = CourseSpeed.from_content(given)
    assert_equal(expected[0], cs.course)
    assert_almost_equal(expected[1], cs.speed, 2)


def test_csespd():
    tests = [
        ((88, 66.67), "088/036"),
        ((None, None), "   /   "),
        ((0, None), "360/   "),
    ]
    for test in tests:
        yield check_csespd_str, test[0], test[1]
        yield check_csespd_parse, test[1], test[0]
