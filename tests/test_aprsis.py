from nose.tools import assert_equal

from apyrs.aprsis import passcode

def check_passcode(callsign, expected):
    assert_equal(passcode(callsign), expected)

def test_passcode():
    tests = [
        ("WB4APR", "16563"),
        ("WB4APR-9", "16563"),
        ("WB4APR-10", "16563"),
        ("AD6NH", "19688"),
        ("AE5PL", "19447"),
        ("N0CALL-20", "13023"),
        ("P1RATE", "9687"),
    ]
    for test in tests:
        yield check_passcode, test[0], test[1]
