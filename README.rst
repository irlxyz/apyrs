APyRS
=====

*APyRS for Python*

Introduction
------------

APyRS is a library for Python 3.7+ for the real-time tactical local information
`Automatic Packet Reporting System <http://aprs.org/>`_ protocol.
