import asyncio
import collections
if False:  # TYPE_CHECKING
    from typing import Deque
from typing import Generator
from typing import Optional


from apyrs.tnc2 import TNC2Frame


def iterbytes(data: bytes) -> Generator[bytes, None, None]:
    """Iterate over bytes one bytes at a time.

    Only required until PEP 0467 exists.

    :param data: bytes to iterate over
    """
    for i in range(len(data)):
        yield data[i:i + 1]


def passcode(callsign: str) -> str:
    """Generate an APRS-IS password for a given callsign.

    If the callsign contains a dash (ASCII 0x2D), then that dash and any
    following characters will be stripped. This allows you to pass a callsign
    with an SSID and still get a valid passcode.

    :param callsign: callsign to be used for login
    """
    callsign_bytes = callsign.split("-")[0].upper().encode("ascii")
    c = iter(callsign_bytes)
    result = 0x73e2
    try:
        while True:
            result ^= next(c) << 8
            result ^= next(c)
    except StopIteration:
        return str(result)


class APRSISClient:
    """A sans-IO APRS-IS client."""

    _overflow: bool
    _buffer: bytes
    _pkts: "Deque[bytes]"

    def __init__(self):
        """Create a new APRSISClient."""
        self._buffer = b""
        self._overflow = False
        self._pkts = collections.deque()

    def _flush(self):
        if len(self._buffer) > 5:
            self._pkts.append(self._buffer)
        self._buffer = b""
        self._overflow = False

    def input(self, data: bytes) -> None:
        """Input received data from the APRS-IS server to the client.

        :param data: the raw data as received
        """
        for c in iterbytes(data):
            if c == b"\r" or c == b"\n":
                self._flush()
            elif not self._overflow:
                self._buffer += c
            if len(self._buffer) > 510:
                self._overflow = True

    def next_pkt(self) -> Optional[TNC2Frame]:
        """Return a pending received frame."""
        try:
            frame = self._pkts.popleft()
            return TNC2Frame.from_bytes(frame)
        except IndexError:
            return None


class AsyncTcpAPRSISClient:
    """An AsyncIO-based TCP APRS-IS client.

    .. note::

       Set passwd to -1 to connect as a read-only user.

    :param login: login user
    :param server: server hostname or IP address
    :param port: server port
    :param passwd: login password
    :param filter: user-defined packet filter
    """

    _client: APRSISClient
    _login: str
    _passwd: str
    _server: str
    _port: int
    _reader: asyncio.StreamReader
    _writer: asyncio.StreamWriter

    def __init__(self, login: str, server: str, port: int, *,
                 passwd: Optional[str] = "-1", filter: str = ""):
        """Create a new AsyncTcpAPRSISClient."""
        self._client = APRSISClient()
        self._login = login
        if passwd is None:
            passwd = passcode(self._login)
        self._passwd = passwd
        self._server = server
        self._port = port
        self._filter = filter

    async def connect(self):
        """Open the connection to the APRS-IS server and login."""
        self._reader, self._writer = await asyncio.open_connection(
            self._server, self._port)
        self._writer.write(
            f"user {self._login} pass {self._passwd} filter {self._filter}\r\n"
            .encode())
        await self._writer.drain()

    async def next_pkt(self):
        """Read the next packet from the APRS-IS server."""
        pkt = self._client.next_pkt()
        if pkt:
            return pkt
        data = await self._reader.read(4096)
        self._client.input(data)
        return self.next_pkt()
