from dataclasses import dataclass

SSID_DESCRIPTION = {
    "0": "Primary",
    "1": "Generic",
    "2": "Generic",
    "3": "Generic",
    "4": "Generic",
    "5": "Cellular Network",
    "6": "Special Activity",
    "7": "Human Portable",
    "8": "Secondary Mobile",
    "9": "Primary Mobile",
    "10": "Internet",
    "11": "Balloons, Aircraft and Spacecraft",
    "12": "One-way Tracker",
    "13": "Weather Station",
    "14": "Tertiary Mobile",
    "15": "Generic",
    "63": "PSK63 HF",
    "tt": "APRStt",
    "ID": "RFID",
    "A": "D-STAR",
    "B": "D-STAR",
    "C": "D-STAR",
    "D": "D-STAR",
    "E": "D-STAR",
    "F": "D-STAR",
    "G": "D-STAR",
    "H": "D-STAR",
    "I": "D-STAR",
    "J": "D-STAR",
    "K": "D-STAR",
    "L": "D-STAR",
    "M": "D-STAR",
    "N": "D-STAR",
    "O": "D-STAR",
    "P": "D-STAR",
    "Q": "D-STAR",
    "R": "D-STAR",
    "S": "D-STAR",
    "T": "D-STAR",
    "U": "D-STAR",
    "V": "D-STAR",
    "W": "D-STAR",
    "X": "D-STAR",
    "Y": "D-STAR",
    "Z": "D-STAR",
}
"""Mapping of SSIDs to their recommended usage."""


@dataclass
class APRSAddress:
    """An APRS entity address or name.

    :param callsign: the callsign
    :param ssid: a one- or two-character SSID
    """

    callsign: str
    """Callsign."""

    ssid: str
    """SSID."""

    @property
    def ssid_description(self) -> str:
        """Description of the recommended SSID usage."""  # noqa: D401
        return SSID_DESCRIPTION.get(self.ssid, "Unknown")

    @classmethod
    def from_string(cls, data: str):  # -> APRSAddress
        """Create a new APRSAddress from a string.

        For example:

        >>> APRSAddress.from_string("MM0ROR-10")
        APRSAddress(callsign='MM0ROR', ssid='10', ssid_description='Internet')

        """
        if data[-2] == "-":
            callsign = data[:-2]
            ssid = data[-1]
        elif data[-3] == "-":
            callsign = data[:-3]
            ssid = data[-2:]
        else:
            callsign = data
            ssid = "0"
        try:
            # Remove leading zero from valid integers
            ssid = str(int(ssid))
        except ValueError:
            pass
        return cls(callsign, ssid)

    def __str__(self) -> str:
        """Return string for with dash seperated callsign and SSID."""
        result = self.callsign
        if self.ssid != "0":
            result += "-" + self.ssid
        return result

    def __repr__(self) -> str:
        """Return representation of APRS address."""
        return (f"APRSAddress(callsign={repr(self.callsign)}, "
                f"ssid={repr(self.ssid)}, "
                f"ssid_description={repr(self.ssid_description)})")
