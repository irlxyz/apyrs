from dataclasses import dataclass
from typing import List
from typing import Tuple

from apyrs.aprs import APRSAddress


def _normalize_path_entry(entry: str) -> Tuple[APRSAddress, bool]:
    if entry[-1] == "*":
        return APRSAddress.from_string(entry[:-1]), True
    return APRSAddress.from_string(entry), False


@dataclass
class TNC2Frame:
    """A "TNC-2" format frame.

    :param srccall: source callsign with optional SSID
    :param dstcall: destination callsign with optional SSID
    :param path: list of tuples of digipeater callsigns with optional SSIDs and
                 a bool to indicate if the hop had been used
    """

    srccall: APRSAddress
    dstcall: APRSAddress
    path: List[Tuple[APRSAddress, bool]]
    info: str

    @classmethod
    def from_bytes(cls, raw_data: bytes):  # -> TNC2Frame
        """Create a new TNC2Frame from raw content.

        :param raw_data: raw frame content
        """
        if len(raw_data) < 5:
            raise ValueError("too short")
        if raw_data[0] == ord("#"):
            raise ValueError("in-band signalling detected")
        data = raw_data.decode("utf-8")
        header, info = data.split(":", 1)
        srccall, path_str = header.split(">", 1)
        path_parts = path_str.split(",")
        dstcall = path_parts.pop(0)
        path = []
        for el in path_parts:
            path.append(_normalize_path_entry(el))
        return cls(APRSAddress.from_string(srccall),
                   APRSAddress.from_string(dstcall),
                   path, info)
