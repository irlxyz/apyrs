from enum import Enum
if False:  # TYPE_CHECKING
    from typing import Literal
    from typing import Tuple


if False:  # TYPE_CHECKING
    Trit = Literal[0, 1, 2]


class MicEMessage(Enum):
    M0 = (0, False, "Off Duty")
    M1 = (1, False, "En Route")
    M2 = (2, False, "In Service")
    M3 = (3, False, "Returning")
    M4 = (4, False, "Committed")
    M5 = (5, False, "Special")
    M6 = (6, False, "Priority")
    C0 = (0, True, "Custom 1")
    C1 = (1, True, "Custom 2")
    C2 = (2, True, "Custom 3")
    C3 = (3, True, "Custom 4")
    C4 = (4, True, "Custom 5")
    C5 = (5, True, "Custom 6")
    C6 = (6, True, "Custom 7")
    EMERGENCY = (7, False, "Emergency")

    def __init__(self, index: int, custom: bool, message: str):
        self.index = index
        self.custom = custom
        self.message = message

    @property
    def trits(self) -> "Tuple[Trit, Trit, Trit]":
        inverse = 7 - self.index
        trit2: "Trit" = (inverse & 1) * (2 if self.custom else 1
                                         )  # type: ignore
        inverse >>= 1
        trit1: "Trit" = (inverse & 1) * (2 if self.custom else 1
                                         )  # type: ignore
        inverse >>= 1
        trit0: "Trit" = (inverse & 1) * (2 if self.custom else 1
                                         )  # type: ignore
        return (trit0, trit1, trit2)


def encode_destination_address_field_byte(lat: str, trit: "Trit") -> str:
    if lat == " ":
        if trit == 2:
            return "K"
        if trit == 1:
            return "Z"
        if trit == 0:
            return "L"
    lat_num = int(lat)
    if trit == 0:
        return chr(ord("0") + lat_num)
    if trit == 1:
        return chr(ord("P") + lat_num)
    if trit == 2:
        return chr(ord("A") + lat_num)


def encode_destination_address_field(lat: str, msg: MicEMessage, north: bool,
                                     lonoff: bool, west: bool, digi: int = 0):
    result: str = ""
    msg_trits: "Tuple[Trit, Trit, Trit]" = msg.trits
    result += encode_destination_address_field_byte(lat[0], msg_trits[0])
    result += encode_destination_address_field_byte(lat[1], msg_trits[1])
    result += encode_destination_address_field_byte(lat[2], msg_trits[2])
    result += encode_destination_address_field_byte(lat[3], 1 if north else 0)
    result += encode_destination_address_field_byte(lat[4], 1 if lonoff else 0)
    result += encode_destination_address_field_byte(lat[5], 1 if west else 0)
    if digi != 0:
        result += f"-{digi:d}"
    return result
