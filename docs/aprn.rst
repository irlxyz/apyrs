Automatic Picture Relay Network
===============================

Automatic Picture Relay Network (APRN) is defined at [aprs-aprn]_.

The :class:`DirectionFieldOfView <apyrs.extension.DirectionFieldOfView>` data
extension is available for this application.
