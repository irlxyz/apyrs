RF Range
========

.. todo::

   As yet this document, nor the code, have considered the content at
   http://aprs.org/aloha.html but they probably should.

APRS allows for the range of stations to be advertised. This information
applies to the radio operating APRS and not necessarily to the station
advertised via :ref:`Automatic Frequency Reporting System <freq>` (AFRS).  For
mobiles and other combined voice/APRS radios, they may be the same, but this
assumption should not be relied upon. AFRS provides the ability to specify a
:ref:`range <freq_range>` for voice stations and repeaters.
