Data Extensions
===============

In APRS, data extensions are a fixed-length 7-byte field that may follow APRS
position data. This extension may be one of the following:

* :ref:`Course and Speed <ext_csespd>`
* :ref:`Wind Direction and Wind Speed <ext_wind>`
* :ref:`Camera Direction and Field of View <ext_dirfov>`
* :ref:`Station Power and Effective Antenna Height/Gain/Directivity <ext_phg>`
* :ref:`Pre-Calculated Radio Range <ext_rng>`
* :ref:`DF Signal Strength and Effective Antenna Height/Gain <ext_dfs>`
* :ref:`Area Object Descriptor <ext_area>`

Some :ref:`lower-level helper functions <ext_helper>` that are used in
implementing the data extensions are also available.

.. _ext_csespd:

Course and Speed
----------------

.. todo::

   Something about DF reports.

.. autoclass:: apyrs.extension.CourseSpeed
   :members:

.. _ext_wind:

Wind Direction and Wind Speed
-----------------------------

.. autoclass:: apyrs.extension.WindDirectionSpeed
   :members:

.. _ext_dirfov:

Camera Direction and Field of View
----------------------------------

This data extension is defined for use with the :doc:`Automatic Picture Relay
Network <aprn>`.

.. autoclass:: apyrs.extension.DirectionFieldOfView
   :members:

.. _ext_phg:

Station Power and Effective Antenna Height/Gain/Directivity
-----------------------------------------------------------

.. todo::

   Probes, DF

.. autoclass:: apyrs.extension.PowerHeightGain
   :members:

.. _ext_rng:

Pre-Calculated Radio Range
--------------------------

.. autoclass:: apyrs.extension.PreCalculatedRange
   :members:

.. _ext_dfs:

DF Signal Strength and Effective Antenna Height/Gain
----------------------------------------------------

.. _ext_area:

Area Object Descriptor
----------------------

.. _ext_helper:

Low-Level Helper Functions
--------------------------

.. autofunction:: apyrs.extension.parse_three_digits

.. autofunction:: apyrs.extension.format_three_digits
