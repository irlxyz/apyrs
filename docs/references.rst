References
==========

.. [aprs101] APRS Working Group, 29 August 2000. `Automatic Position Reporting
   System Protocol Reference Version 1.0.1
   <http://www.aprs.org/doc/APRS101.PDF>`_.

.. [aprs-aprn] Bob Bruninga. `Automatic Picture Relay Network
   <http://www.aprs.org/aprn.html>`_.

.. [aprs-phg] Bob Bruninga. `The Importance of PHG RF Range Circles
   <http://aprs.org/phg.html>`_.

.. [phg-mobile] Bob Bruninga. `APRS Mobile Range
   <http://aprs.org/aprsdos-pix/PHGmobile.txt>`_.

.. [newn-probes] Bob Bruninga, 12 January 2009. `APRS Network Performance
   Measurement (PHGR) <http://aprs.org/newN/probes.txt>`_. (Replaced `original
   proposal for APRS 1.2 <http://aprs.org/aprs12/probes.txt>).
